export class MysqlConfig {
    port: number = 0;
    hostname: string = "";
    username: string = "";
    password: string = "";
    database: string = "";
}