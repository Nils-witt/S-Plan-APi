export class WebServerConfig {
    port: number = 3000;
    apiDocumentation: boolean = false;
    serverOrigin: string = "";
}