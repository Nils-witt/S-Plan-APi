export class PushFrameworksConfig {
    telegramBot: boolean = false;
    telegramBotToken: string = "";
    sendgridToken: string = "";
    firebaseCertificatePath: string = "";
    vapidKeyPublic: string = "";
    vapidKeyPrivate: string = "";
    vapidKeyMail: string = "";

}